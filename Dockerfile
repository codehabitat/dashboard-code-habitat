FROM node:alpine

RUN mkdir -p /usr/src/app
COPY ./dist /usr/src/app
WORKDIR /usr/src/app

RUN yarn install --production=true --silent --pure-lockfile

RUN yarn global add http-server

CMD ["hs", "./", "-p", "4000"]

EXPOSE 4000