# Dashboard Code Habitat

## Docker

1. **Build** images

	``` bash
	$ docker build -t dashboard-habitat .
	```

2. **Run** images

	``` bash
	$ docker run -it -p 4000:4000 dashboard-habitat
	```
		